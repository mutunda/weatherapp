// The Vue build version to load with the `import` command
// (runtime-only or standalone) has been set in webpack.base.conf with an alias.
import Vue from 'vue'
import App from './App'
import router from './router'
import _ from 'lodash'
import vueResouece from 'vue-resource';
import {store} from './store';


Vue.config.productionTip = false;
Vue.use(vueResouece);
Vue.filter('uppercase', function (value) {
  return _.upperFirst(value);
});

/* eslint-disable no-new */
new Vue({
  el: '#app',
  router,
  store,
  template: '<App/>',
  components: { App }
})
