import Vue from 'vue'
import Router from 'vue-router'
import Search from '@/components/Search'
import Show from '@/components/Show'


Vue.use(Router)

export default new Router({
  routes: [
    {
      path: '/',
      name: 'search',
      component: Search
    },
    {
      path:'/show',
      name: 'show',
      component: Show
    }
  ]
})
