import Vue from 'vue';
import Vuex from 'vuex';
import axios from 'axios';
import createPersistedState from 'vuex-persistedstate'

import {API_KEY} from '../config/api'

Vue.use(Vuex);

export const store = new Vuex.Store({
  plugins:[createPersistedState()],
  state: {
    currentLocation: {},
    dataForGivenLocation: {},
    dataForCurrTime: {},
    dataForGivenLocationF:{},
    dataForCurrTimeF:{},
    unitOfMeasure:false,
    loading: false

  },
  mutations: {

    getByGivenLocation(state, payload){
      localStorage.clear();
      state.dataForCurrTime = payload.sdata;
      state.dataForGivenLocation = payload.gdata;
      state.dataForCurrTimeF = payload.sdataF;
      state.dataForGivenLocationF = payload.gdataF;
      localStorage.setItem('state', JSON.stringify(state));
    },
    getByMyGeolocation(state, payload){
      localStorage.clear();
      state.dataForCurrTime = payload.sdata;
      state.dataForGivenLocation = payload.gdata;
      state.dataForCurrTimeF = payload.sdataF;
      state.dataForGivenLocationF = payload.gdataF;
      localStorage.setItem('state', JSON.stringify(state));
    },
    toCtoF(state, payload){
      state.unitOfMeasure = payload.temperatureUnit;
    },

    changeState(state,payload){
      state = payload.storedState;
    }
  },
  getters:{
    toCtoF(state){
      if(state.unitOfMeasure === false){
        return {
          generalForecast: state.dataForGivenLocation,
          hourlyForecast: state.dataForCurrTime,
        }
      } else {
        return {
          generalForecast: state.dataForGivenLocationF,
          hourlyForecast: state.dataForCurrTimeF,
        }
      }
    }
  },

  actions: {
    getByGivenLocation: async ({commit, state}, payload) => {

      state.loading = !state.loading;

      const gdata = await axios.get(`http://api.openweathermap.org/data/2.5/forecast/daily?q=${payload.location}&units=metric&cnt=7&APPID=${API_KEY}`)
        .then((res) => res.data)
        .catch((error) => console.log(error));

      const sdata = await axios.get(`http://api.openweathermap.org/data/2.5/weather?q=${payload.location}&units=metric&APPID=${API_KEY}`)
        .then((res) => res.data)
        .catch((error) => console.log(error));


      const gdataF = await axios.get(`http://api.openweathermap.org/data/2.5/forecast/daily?q=${payload.location}&units=imperial&cnt=7&APPID=${API_KEY}`)
        .then((res) => res.data)
        .catch((error) => console.log(error));


      const sdataF = await axios.get(`http://api.openweathermap.org/data/2.5/weather?q=${payload.location}&units=imperial&APPID=${API_KEY}`)
        .then((res) => res.data)
        .catch((error) => console.log(error));
      state.loading = !state.loading;

      commit({
        type: 'getByGivenLocation',
        gdata,
        sdata,
        gdataF,
        sdataF
      });
    },
    getByMyGeolocation({commit,state}){
      state.loading = !state.loading;
      navigator.geolocation.getCurrentPosition(
        async (coordinates) => {
          const gdata = await axios.get(`http://api.openweathermap.org/data/2.5/forecast/daily?lat=${coordinates.coords.latitude}&lon=${coordinates.coords.longitude}&units=metric&cnt=7&APPID=${API_KEY}`)
            .then(res => res.data)
            .catch(error => error);

          const sdata = await axios.get(`http://api.openweathermap.org/data/2.5/weather?lat=${coordinates.coords.latitude}&lon=${coordinates.coords.longitude}&units=metric&APPID=${API_KEY}`)
            .then(res => res.data)
            .catch(error => error);

          const gdataF = await axios.get(`http://api.openweathermap.org/data/2.5/forecast/daily?lat=${coordinates.coords.latitude}&lon=${coordinates.coords.longitude}&units=imperial&cnt=7&APPID=${API_KEY}`)
            .then(res => res.data)
            .catch(error => error);

          const sdataF = await axios.get(`http://api.openweathermap.org/data/2.5/weather?lat=${coordinates.coords.latitude}&lon=${coordinates.coords.longitude}&units=imperial&APPID=${API_KEY}`)
            .then(res => res.data)
            .catch(error => error);

          state.loading = !state.loading;

          commit({
            type: 'getByMyGeolocation',
            gdata,
            sdata,
            gdataF,
            sdataF
          })
        },
        (error) => {
          commit({
            type: 'getByMyGeolocation',
            error
          });
        })
    }
  },
});
